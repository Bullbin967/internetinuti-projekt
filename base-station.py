from multiprocessing import Process, Pipe, Pool
from circuitpython_nrf24l01.rf24 import RF24
import board
import busio
import digitalio as dio
import time
import struct
import argparse
import math
import multiprocessing

from random import randint
import numpy as np

from tuntap import TunTap
from scapy.all import *


SPI0 = {
    'MOSI':10,#dio.DigitalInOut(board.D10),
    'MISO':9,#dio.DigitalInOut(board.D9),
    'clock':11,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D17),
    'csn':dio.DigitalInOut(board.D8),
    }
SPI1 = {
    'MOSI':20,#dio.DigitalInOut(board.D10),
    'MISO':19,#dio.DigitalInOut(board.D9),
    'clock':21,#dio.DigitalInOut(board.D11),
    'ce_pin':dio.DigitalInOut(board.D27),
    'csn':dio.DigitalInOut(board.D18),
    }
  
iface = 'LongGe'

tun = TunTap(nic_type='Tun', nic_name='tun0')

tun.config(ip='192.168.1.11', mask='255.255.255.0')
tun_test_string="kadssdffdsssssssssssssssssssssssssssssssssssssfdd"
#måste sätta global inuti funktionerna för att denna ska uppdateras
#global q=0
ret=""
size = 1500

buf2 = tun.read(size)
  
def tx(nrf, channel, address, count, size, fragments_list_to_send):
    nrf.open_tx_pipe(address)  # set address of RX node into a TX pipe
    nrf.listen = False
    nrf.channel = channel
    count2=int(fragments_list_to_send[0]) #takes out how many fragments to send are used in the while loop
    status = []
    #buffer = np.random.bytes(size) #creates random byte payload as test is sent np = numpy so random number.
    
    #def tunfStart():
    str3=str(fragments_list_to_send[0]) #convert the number of fragments to int from string to be able to use it in the forloop
    fragments_list_to_send[0]=str3
    start = time.monotonic()
    for i in range(count2+1):
        # use struct.pack to packetize your data
        # into a usable payload
        #buffer = struct.pack(<, count)
        # 'i' means a single 4 byte int value.
        # '<' means little endian byte order. this may be optional
        #print("Sending: {} as struct: {}".format(count, buffer))
        buffer=fragments_list_to_send[i] #sends first the number of fragments expected, then all fragments
        buffer=str.encode(buffer) #encode it back to clean bytes so you can send it.
        #buffer_pack=struckt.pack(buffer)
        result = nrf.send(buffer) ##ends the payload.
        if not result:
            #print("send() failed or timed out")
            #print(nrf.what_happened())
            status.append(False)
        else:
            #print("send() successful")
            status.append(True)
        # print timer results despite transmission success
        
    total_time = time.monotonic() - start



    print('{} successfull transmissions, {} failures, {} bps'.format(sum(status), len(status)-sum(status), size*8*len(status)/total_time))
    #q=1

def rx(nrf, channel, address, count, queue):
    nrf.open_rx_pipe(0, address)
    nrf.listen = True  # put radio into RX mode and power up
    nrf.channel = channel

    print('Rx NRF24L01+ started w/ power {}, SPI freq: {} hz'.format(nrf.pa_level, nrf.spi_frequency))

    received = []
    str_recieved_list=[]

    start_time = None
    start = time.monotonic()
    count3=0
    #nbr_fragments=0
    while True:

        if nrf.update() and nrf.pipe is not None:
            if start_time is None:
                start_time = time.monotonic()
            # print details about the received packet
            # fetch 1 payload from RX FIFO
            received.append(nrf.any())
            rx = nrf.read()  # also clears nrf.irq_dr status flag
            # expecting an int, thus the string format '<i'
            # the rx[:4] is just in case dynamic payloads were disabled
            #buffer = struct.unpack("<i", rx[:4])  # [:4] truncates padded 0s
            # print the only item in the resulting tuple from
            # using `struct.unpack()`
            #print("Received: {}, Raw: {}".format(buffer[0], rx))
            #print("Received: {}, Raw: {}", rx)
            #print("BYTESEN TILLBAKA TILL EN STRING = " + rx.decode("utf8")) #vill skriva detta till tun dock som byt
            #kopiera in den funktionen du hade innan fast gör så den sötter ihop strängarna som kommer in till ett paket igen.
            

        
            #ime.sleep(1/10)
            str_recieved_list.append(rx.decode("utf8"))
           
            #print(str_recieved_list)
            #print(received)
            #if(str_recieved_list is not None):

            nbr_fragments=int(str_recieved_list[0])
            #print(nbr_fragments)
            #nbr_fragments=4
            temp = []
     
            for x in str_recieved_list:
                if x not in temp:
                    temp.append(x)
            #print(temp)
            
            str_recieved_list=temp
            #print(str_recieved_list)
            #print('antal counts; ',count3)
            #print('antal fragments; ',nbr_fragments)
            #if(count3>nbr_fragments-1):#behöver ingen counter för vänta tills vi får alla segmenten bara och sen break.
                #break
            #
            if(len(str_recieved_list)==7): #om vi har 7 segment break.
                break 
            
                
            #buf_b = hex_bytes(rx)
            #tun.write(buf_b)

            #start = time.monotonic()
            #if(count3>nbr_fragments):
                #start=1001

            count3 +=1
            # this will listen indefinitely till count == 0
    total_time = time.monotonic() - start_time
    reacembled_packet=""
    nbr_fragments=int(str_recieved_list[0])
    #str_recieved_list = list(set(str_recieved_list))
    
    
    temp = []
    temp=str_recieved_list
     
    #for x in str_recieved_list:
        #if x not in temp:
            #temp.append(x)
    #print(temp) #fragmenterade listan.
    #str_recieved_list= temp
    temp.pop(0) #tar bort counten som gav antal fragments.
    #print("".join(temp)) ##funkar!
    str_write="".join(temp)
    #str_write_tun=str.encode(str_write)
    #global tun_test_string
    tun_test_string=str_write
    #print("kkaee")
    #print(tun_test_string)
    str_write_tun=str_write.encode('utf-8') #converting fromm string to bytes
    #str_write_tun=str_write.encode('utf-16')
    #tun_test_string=str_write_tun.hex()
    #tun_test_string=bytearray(tun_test_string.encode())
    #str_write_tun=
    str_write_tun=hex_bytes(str_write_tun) #convert to hexa before writing tun
    #print(tun_test_string)
    print(str_write_tun)

    tun.write(str_write_tun)
    #print(tun.read(1500))
    
    #tun.write(str_write_tun)
    #for i in range(1,nbr_fragments): ##loopar från 1 till nbr fragments, tar bort den siffran som jag la till med antalet fragments.
        
       # reacembled_packet=reacembled_packet + temp[i] ##byt tillbaka till str_recieved_list[i] om detta inte funkar
    #reacembled_fin=str.encode(reacembled_packet) ## convert string to bytes
    
    #print(reacembled_packet)
    #tun.write(reacembled_fin)## write to tun again.

    #q==2
    print('{} received, {} average, {} bps'.format(len(received), np.mean(received), np.sum(received)*8/total_time))

    #returnar stringen som recevades till main och vidare till tunStart för att sändas.
    #return str_write
    ret=queue.get()
    queue.put(tun_test_string)
    ##conn.send(tun_test_string)
    #conn.close()

def tunfStart(return_string_from_rx):

##kanske får lägga hela denna metoden i main så jag kan skicka vidare paketen in i send metodeon.


# Create and configure a TUN interface

    #tun = TunTap(nic_type='Tun', nic_name='longge')

#tap = TunTap(nic_type='Tap', nic_name='tap0')

    #tun.config(ip='11.11.11.2', mask='255.255.255.0', gateway='11.11.11.1')



#Read from TUN interface
 
    size = 1500
    while True:
        buf = tun.read(size)

    #Write to TUN interface

        #tun.write(buf)

        addr = tun.ip

        #print(addr)

        print(buf)

        buf_x = bytes_hex(buf)

        print(buf_x) #detta printar paketet som en string.
        #buf_x=tun_test_string    
        #buf_x="Omer Abdelatif Mohamed Nour!!!!!!!!!!!!!!!!!!!!!"
        #str_buf_x=buf_x #test stringen är redan en string
        str_buf_x=buf_x.decode("utf8")#lägger in bytsen som en string
        print('Read tun; ',str_buf_x)
        print(str_buf_x[0 : 3])
        print('size packet, ping is 168 bytes; ', len(str_buf_x))
        if(str_buf_x[ 0 : 3]=="450" and len(str_buf_x)==168 ):#om det är ett ping packet till google. break. ip 192.168.1.3 longge4
            
        


            size_packet= len(str_buf_x)#så varje char i stringen tar upp en byte. kollar storleken på paketet
            print(size_packet)
            
            print("nu är det en string ist för byte array " + str_buf_x)
            if((size_packet)>32):
                nbr_pakets=math.ceil(size_packet/32) ##rundar upp antalet gånger vi måste splitta upp det, så 5.3 blir 6 paket. payload =24

                return_list=[]
                return_list.append(nbr_pakets) ##lägger in antal fragments som ska skickas.
                list1 = list(str_buf_x)#lägger in hela stringen från tun i en lsita så varje string/char läggs in i varje slot/index.
                #print(list1)        
                fragmented_payload="" #string som kommer hålla det vi ska returna.
                print('Fragements that will be sent = ' + str(nbr_pakets))
                print(nbr_pakets)
                print(size_packet)
                while(nbr_pakets>0):
                    #print(nbr_pakets)

                    for i in range(32): #Loopar från 0->31 dvs 32bitar.
                        #print(list1.pop(0)) 
                        if len(list1)==0:
                            nbr_pakets = 0## ser till så vi bryter while loopen
                            break   ## om listan är tom så break for loopen.
                        
                        fragmented_payload=fragmented_payload+list1.pop(0) ## detta bör funka för att ta ut första char och deleta varje som popas, i listan, och appenda till en enda string som kan skickas.
                        
                        if(i==31): 
                            nbr_pakets -= 1##minskar antalet loopningar.
                            print(nbr_pakets)
                            #print(fragmented_payload)
                            break   
                    
                    return_list.append(fragmented_payload) ##lägger in alla fragments som ska skicka i en lista.
                    print(fragmented_payload)
                    fragmented_payload=""
                print(return_list)
            
            else: #om paketet är mindre än 32
                return(str_buf_x) 
            #list1 = list("kalle")
            #print(size_packet) #så varje char i stringen tar upp en byte.
            return return_list

    #list1 = list("kalle")
    #print(size_packet) #så varje char i stringen tar upp en byte.

    #buf_b = hex_bytes(buf_x)

    #print(buf_b)
    



            


    #buf_b = hex_bytes(buf_x)

        
    #print(buf_b)
    
#tun.close()
    # Close and destroy interface




if __name__ == "__main__":
    while True:
        parser = argparse.ArgumentParser(description='NRF24L01+ test')
        parser.add_argument('--src', dest='src', type=str, default='me', help='NRF24L01+\'s source address') #detta är bara för att skicka ut datan till terminalen "parser" fast det man knappar in här, ex kanal 97 används längre ner i process för att skicka
        parser.add_argument('--dst', dest='dst', type=str, default='me', help='NRF24L01+\'s destination address')
        parser.add_argument('--count', dest='cnt', type=int, default=10, help='Number of transmissions')
        parser.add_argument('--size', dest='size', type=int, default=32, help='Packet size')
        parser.add_argument('--txchannel', dest='txchannel', type=int, default=97, help='Tx channel', choices=range(0,125))
        parser.add_argument('--rxchannel', dest='rxchannel', type=int, default=96, help='Rx channel', choices=range(0,125))

        args = parser.parse_args()

        SPI0['spi'] = busio.SPI(**{x: SPI0[x] for x in ['clock', 'MOSI', 'MISO']})
        SPI1['spi'] = busio.SPI(**{x: SPI1[x] for x in ['clock', 'MOSI', 'MISO']})

        #que_1=multiprocessing.Queue()
        #tunfStart():
        count1=1
        queue1=multiprocessing.Queue()
        queue1.put(ret)
        #child_conn = Pipe()
        #parent_conn= Pipe()
        ## initialize the nRF24L01 on the spi bus object
        # rx_nrf = RF24(**{x: SPI0[x] for x in ['spi', 'csn', 'ce']})
        # tx_nrf = RF24(**{x: SPI1[x] for x in ['spi', 'csn', 'ce']})

        rx_nrf = RF24(SPI0['spi'], SPI0['csn'], SPI0['ce_pin'])
        rx_nrf.data_rate = 1
        rx_nrf.auto_ack = True
            #nrf.dynamic_payloads = True
        rx_nrf.payload_length = 32
        rx_nrf.crc = True
        rx_nrf.ack = 1
        rx_nrf.spi_frequency = 20000000
        
        rx_process = Process(target=rx, kwargs={'nrf':rx_nrf, 'address':bytes(args.src, 'utf-8'), 'count': count1, 'channel': args.rxchannel,'queue': queue1}) ##här hämtar man antal paket från parser fältetn och vilken kanal man ställt in från parser fälten.
        #while (q==1):
        
        
        rx_process.start()
        #a=parent_conn
        #print(a)

        rx_process.join()
        
        #pool = multiprocessing.Pool(1) # Just a one-process Pool.
        ##result = pool.apply_async(rx,0, 0, 0, 96)
        #try:
        #    print(result.get()) # Wait 10 seconds
        #except multiprocessing.TimeoutError:
        #    print("Timed out")
        #    #pool.terminate() # Kill all (1, in this case) processes in the pool
        #    pool.join()

        print("Main, soon sending reply back to mobile")
        #print(queue1.get())
        string_to_send_back=queue1.get()
        #print(result.get())
        #print(result)
    

        #print(que_1.get())
        #return_recieved_string=rx()
        #time.sleep(1)
        fragments_list=tunfStart(string_to_send_back)   #### kommetera bort denna på receivern
        

        tx_nrf = RF24(SPI1['spi'], SPI1['csn'], SPI1['ce_pin'])

        tx_nrf.data_rate = 1
        tx_nrf.auto_ack = True
            #nrf.dynamic_payloads = True
        tx_nrf.payload_length = 32
        tx_nrf.crc = True
        tx_nrf.ack = 1
        tx_nrf.spi_frequency = 20000000


        tx_process = Process(target=tx, kwargs={'nrf':tx_nrf, 'address':bytes(args.dst, 'utf-8'), 'count': args.cnt, 'channel': args.txchannel, 'size':args.size,'fragments_list_to_send':fragments_list})
        tx_process.start()
        tx_process.join()

  
